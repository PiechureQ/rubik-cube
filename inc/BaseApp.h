#ifndef RUBIKS_BASEAPP_H
#define RUBIKS_BASEAPP_H

#include <iostream>
#include "OGRE/Ogre.h"
#include "OGRE/Bites/OgreApplicationContext.h"

class BaseApp : public OgreBites::ApplicationContext{
public:
    explicit BaseApp(Ogre::String name);;
    void setup(void);
};


#endif //RUBIKS_BASEAPP_H
