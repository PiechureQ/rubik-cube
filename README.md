# Rubik's cube
Simple rubik's cube representation using OGRE3D. 
Project created to pass the semester.

### Dependencies
 - [`OGRE3D`](https://www.ogre3d.org/) - all visual things
 - [`OIS`](https://wgois.github.io/OIS/) - handling input

License
----

MIT