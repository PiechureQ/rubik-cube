#include <iostream>
#include "inc/BaseApp.h"

int main() {
    BaseApp app("Nice gosciu");
    app.initApp();
    app.getRoot()->startRendering();
    app.closeApp();
    return 0;
}