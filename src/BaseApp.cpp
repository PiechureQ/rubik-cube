#include "../inc/BaseApp.h"
#include "OGRE/Ogre.h"
#include "OGRE/Bites/OgreApplicationContext.h"

using namespace Ogre;

BaseApp::BaseApp(String name) : OgreBites::ApplicationContext(name){}

ManualObject* createStickerMesh(String name, String matName){
    ManualObject* sticker = new ManualObject(name);

    sticker->begin(matName);

    sticker->position(-1, 1, 0);
    sticker->normal(0, 0, 1);
    sticker->textureCoord(0, 0);
    sticker->position(-1, -1, 0);
    sticker->normal(0, 0, 1);
    sticker->textureCoord(0, 1);
    sticker->position(1, -1, 0);
    sticker->normal(0, 0, 1);
    sticker->textureCoord(1, 1);
    sticker->position(1, 1, 0);
    sticker->normal(0, 0, 1);
    sticker->textureCoord(1, 0);

    sticker->quad(0, 1, 2, 3);

    sticker->end();
    return sticker;
}

ManualObject* createCubeMesh(String name, String mat) {

    ManualObject* cube = new ManualObject(name);
    cube->begin(mat);

    cube->position(1,-1,1);cube->normal(0.408248,-0.816497,0.408248);cube->textureCoord(1,0);
    cube->position(-1,-1,-1);cube->normal(-0.408248,-0.816497,-0.408248);cube->textureCoord(0,1);
    cube->position(1,-1,-1);cube->normal(0.666667,-0.333333,-0.666667);cube->textureCoord(1,1);
    cube->position(-1,-1,1);cube->normal(-0.666667,-0.333333,0.666667);cube->textureCoord(0,0);
    cube->position(1,1,1);cube->normal(0.666667,0.333333,0.666667);cube->textureCoord(1,0);
    cube->position(-1,-1,1);cube->normal(-0.666667,-0.333333,0.666667);cube->textureCoord(0,1);
    cube->position(1,-1,1);cube->normal(0.408248,-0.816497,0.408248);cube->textureCoord(1,1);
    cube->position(-1,1,1);cube->normal(-0.408248,0.816497,0.408248);cube->textureCoord(0,0);
    cube->position(-1,1,-1);cube->normal(-0.666667,0.333333,-0.666667);cube->textureCoord(0,1);
    cube->position(-1,-1,-1);cube->normal(-0.408248,-0.816497,-0.408248);cube->textureCoord(1,1);
    cube->position(-1,-1,1);cube->normal(-0.666667,-0.333333,0.666667);cube->textureCoord(1,0);
    cube->position(1,-1,-1);cube->normal(0.666667,-0.333333,-0.666667);cube->textureCoord(0,1);
    cube->position(1,1,-1);cube->normal(0.408248,0.816497,-0.408248);cube->textureCoord(1,1);
    cube->position(1,-1,1);cube->normal(0.408248,-0.816497,0.408248);cube->textureCoord(0,0);
    cube->position(1,-1,-1);cube->normal(0.666667,-0.333333,-0.666667);cube->textureCoord(1,0);
    cube->position(-1,-1,-1);cube->normal(-0.408248,-0.816497,-0.408248);cube->textureCoord(0,0);
    cube->position(-1,1,1);cube->normal(-0.408248,0.816497,0.408248);cube->textureCoord(1,0);
    cube->position(1,1,-1);cube->normal(0.408248,0.816497,-0.408248);cube->textureCoord(0,1);
    cube->position(-1,1,-1);cube->normal(-0.666667,0.333333,-0.666667);cube->textureCoord(1,1);
    cube->position(1,1,1);cube->normal(0.666667,0.333333,0.666667);cube->textureCoord(0,0);

    cube->triangle(0,1,2);		cube->triangle(3,1,0);
    cube->triangle(4,5,6);		cube->triangle(4,7,5);
    cube->triangle(8,9,10);		cube->triangle(10,7,8);
    cube->triangle(4,11,12);	cube->triangle(4,13,11);
    cube->triangle(14,8,12);	cube->triangle(14,15,8);
    cube->triangle(16,17,18);	cube->triangle(16,19,17);
    cube->end();

    return cube;

}


void BaseApp::setup(void){
    // do not forget to call the base first
    OgreBites::ApplicationContext::setup();

    // get a pointer to the already created root
    Root* root = getRoot();
    SceneManager* scnMgr = root->createSceneManager();
    // register our scene with the RTSS
    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    // additional light
    Light* spotLight = scnMgr->createLight("SpotLight");
    SceneNode* subLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    subLightNode->setPosition(-1, 5, 2);
    subLightNode->lookAt(Vector3(0, 0, 0), Node::TS_PARENT);
    subLightNode->attachObject(spotLight);

    //base light
    Light* light = scnMgr->createLight("MainLight");
    light->setType(Light::LT_DIRECTIONAL);
    SceneNode* lightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    lightNode->setPosition(10, 20, 5);
    lightNode->lookAt(Vector3(0, 0, 0), Node::TS_PARENT);
    lightNode->attachObject(light);

    // also need to tell where we are
    SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->setPosition(0, 10, 20);
    camNode->lookAt(Vector3(0, 0, 0), Node::TS_PARENT);

    // create the camera
    Camera* cam = scnMgr->createCamera("myCam");
    cam->setNearClipDistance(1); // specific to this sample
    cam->setAutoAspectRatio(true);
    camNode->attachObject(cam);

    // and tell it to render into the main window
    Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(ColourValue(0.6, 0.9, 1.0));

    SceneNode* node = scnMgr->getRootSceneNode()->createChildSceneNode();
    SceneNode* sticker = new SceneNode(scnMgr);
    node->addChild(sticker);

    MeshPtr cubeMesh = createCubeMesh("cube", "cube/black")->convertToMesh("cube");
    MeshPtr stickerMesh = createStickerMesh("stickerR", "sticker/red")->convertToMesh("sticker");

    Entity* cube = scnMgr->createEntity("cube", "cube");
    Entity* stickerR = scnMgr->createEntity("sticker", "sticker");

    node->attachObject(cube);
    sticker->attachObject(stickerR);
    sticker->setPosition(Vector3(0, 0, 1.1));
    node->scale(Real(2), Real(2), Real(2));
}